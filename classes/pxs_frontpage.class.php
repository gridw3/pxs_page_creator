<?php

class PXS_Frontpage {
    
    public static function create() {
        if(!PXS_Frontpage::exists('PXS_Frontpage')){
            $frontpage = array(
                'post_title'    => 'PXS_Frontpage',
                'post_content'  => '[pxs_page_form]',
                'post_status'   => 'publish',
                'post_type'     => 'page',
            );
            wp_insert_post($frontpage);
            PXS_Frontpage::set_as_front('PXS_Frontpage');
        }
    }

    private static function exists ($title) {
        $page = get_page_by_title($title);
        return !$page ? false : true;
    }

    private static function set_as_front ($title) {
        $page = get_page_by_title($title);
        update_option( 'page_on_front', $page->ID );
        update_option( 'show_on_front', 'page' );
    }

}