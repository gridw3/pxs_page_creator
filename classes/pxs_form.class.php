<?php

class PXS_Form {
    
    public static function generate() {
        $form = file_get_contents( __DIR__ . '/../views/page-form.php', true );
        $editor = PXS_Form::generate_editor();

        $form = str_replace('{{CONTENT_EDITOR}}', $editor, $form);
        $form = str_replace('{{ADMIN_URL}}', esc_url( admin_url('admin-post.php') ), $form);
        
        return $form;
    }

    public static function generate_editor() {
        ob_start();
        wp_editor( '', 'pxs_form_editor' );
        $editor_content = ob_get_clean();
        return $editor_content;
    }

    public static function save() {
        $new_page = array(
            'post_title'    => $_POST['pxs_form_title'],
            'post_excerpt'  => $_POST['pxs_form_excerpt'],
            'post_content'  => $_POST['pxs_form_editor'],
            'post_status'   => 'publish',
            'post_type'     => 'page'
        );
        $page_id = wp_insert_post($new_page);
        PXS_Form::set_thumbnail($page_id, $_FILES);
    }

    public static function set_thumbnail($page_id, $files) {
        if ( !empty( $files['pxs_thumbnail']['name'] ) ) {
            $supported_types = array( 'image/png', 'image/jpeg', 'image/gif' );
            $arr_file_type = wp_check_filetype( basename( $files['pxs_thumbnail']['name'] ) );
            $uploaded_type = $arr_file_type['type'];
    
            if ( in_array( $uploaded_type, $supported_types ) ) {
                $upload = wp_upload_bits($files['pxs_thumbnail']['name'], null, file_get_contents($files['pxs_thumbnail']['tmp_name']));

                if ( !$upload['error'] ) {
                    $attachment = array(
                        'post_mime_type' => $upload['type'],
                        'post_title'     => sanitize_file_name($files['pxs_thumbnail']['name']),
                        'post_content'   => '',
                        'post_status'    => 'inherit'
                    );
    
                    $attach_id = wp_insert_attachment( $attachment, $upload['url']);
                    $attach_data = wp_generate_attachment_metadata( $attach_id, $upload['url'] );
                    wp_update_attachment_metadata( $attach_id, $attach_data );
                    set_post_thumbnail( $page_id, $attach_id );
                }
            }
        }

        PXS_Form::exit();
    }

    public static function exit() {
        wp_redirect(home_url());
    }

}