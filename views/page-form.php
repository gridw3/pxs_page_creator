<form action="{{ADMIN_URL}}" method="post" enctype="multipart/form-data" class="pxs-form">
    
    <div class="form-group">
        <label>Título da Página</label>
        <div class="pxs-tooltip">
            <span class="pxs-tooltip__icon">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm0 18.25c-.69 0-1.25-.56-1.25-1.25s.56-1.25 1.25-1.25c.691 0 1.25.56 1.25 1.25s-.559 1.25-1.25 1.25zm1.961-5.928c-.904.975-.947 1.514-.935 2.178h-2.005c-.007-1.475.02-2.125 1.431-3.468.573-.544 1.025-.975.962-1.821-.058-.805-.73-1.226-1.365-1.226-.709 0-1.538.527-1.538 2.013h-2.01c0-2.4 1.409-3.95 3.59-3.95 1.036 0 1.942.339 2.55.955.57.578.865 1.372.854 2.298-.016 1.383-.857 2.291-1.534 3.021z"/></svg>
            </span>
            <span class="pxs-tooltip__description">Título na nova página</span>
        </div>
        <input type="text" name="pxs_form_title" class="form-control" placeholder="Minha página">
    </div>

    <div class="form-group">
        <label>Resumo</label>
        <div class="pxs-tooltip">
            <span class="pxs-tooltip__icon">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm0 18.25c-.69 0-1.25-.56-1.25-1.25s.56-1.25 1.25-1.25c.691 0 1.25.56 1.25 1.25s-.559 1.25-1.25 1.25zm1.961-5.928c-.904.975-.947 1.514-.935 2.178h-2.005c-.007-1.475.02-2.125 1.431-3.468.573-.544 1.025-.975.962-1.821-.058-.805-.73-1.226-1.365-1.226-.709 0-1.538.527-1.538 2.013h-2.01c0-2.4 1.409-3.95 3.59-3.95 1.036 0 1.942.339 2.55.955.57.578.865 1.372.854 2.298-.016 1.383-.857 2.291-1.534 3.021z"/></svg>
            </span>
            <span class="pxs-tooltip__description">Texto resumido</span>
        </div>
        <textarea name="pxs_form_excerpt" class="form-control"></textarea>
    </div>
    
    <div class="form-group">
        <label>Conteúdo</label>

        <div class="pxs-options">
            <label class="pxs-options__option">
                <input type="radio" name="pxs_content_type" class="pxs-options__input" checked="checked">
                <div class="pxs-options__content">
                    <span class="pxs-options__label">Em branco</span>
                </div>
            </label>

            <label class="pxs-options__option">
                <input type="radio" name="pxs_content_type" class="pxs-options__input">
                <div class="pxs-options__content">
                    <span class="pxs-options__label">HTML</span>
                    <div class="pxs-options__detail">
                        {{CONTENT_EDITOR}}
                    </div>
                </div>
            </label>
        </div>

        
    </div>

    <div class="form-group">
        <label>Thumbnail</label>
        <div class="pxs-tooltip">
            <span class="pxs-tooltip__icon">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm0 18.25c-.69 0-1.25-.56-1.25-1.25s.56-1.25 1.25-1.25c.691 0 1.25.56 1.25 1.25s-.559 1.25-1.25 1.25zm1.961-5.928c-.904.975-.947 1.514-.935 2.178h-2.005c-.007-1.475.02-2.125 1.431-3.468.573-.544 1.025-.975.962-1.821-.058-.805-.73-1.226-1.365-1.226-.709 0-1.538.527-1.538 2.013h-2.01c0-2.4 1.409-3.95 3.59-3.95 1.036 0 1.942.339 2.55.955.57.578.865 1.372.854 2.298-.016 1.383-.857 2.291-1.534 3.021z"/></svg>
            </span>
            <span class="pxs-tooltip__description">Imagem da página</span>
        </div>
        <input type="file" name="pxs_thumbnail" size="25"  class="form-control" />
    </div>
    
    <input type="hidden" name="action" value="save_page">
    <button type="submit" class="btn btn-primary pxs-button">Criar página</button>
    
</form>