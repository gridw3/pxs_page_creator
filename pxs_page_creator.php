<?php
/*
Plugin Name: Pyxys Page Creator
Description: Simple Page Creation Plugin.
Version: 1.0.0
Author: Leonardo Ruiz
Text Domain: pxs_page_creator
*/

// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}

require_once( 'classes/pxs_page_creator.class.php' );
require_once( 'classes/pxs_frontpage.class.php' );
require_once( 'classes/pxs_form.class.php' );
require_once( 'includes/enqueue.php' );

register_activation_hook( __FILE__, array( 'PXS_Page_Creator', 'plugin_activation' ) );

add_action( 'wp_enqueue_scripts', 'pxs_enqueue' );
add_action( 'pxs_create_frontpage', array( 'PXS_Frontpage', 'create' ) );
add_action( 'admin_post_nopriv_save_page', array( 'PXS_Form', 'save' ) );
add_action( 'admin_post_save_page', array( 'PXS_Form', 'save' ) );

add_shortcode( 'pxs_page_form', array( 'PXS_Form', 'generate' ) );