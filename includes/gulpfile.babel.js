'use strict';

import gulp from 'gulp';
import stylus from 'gulp-stylus';
import watch from 'gulp-watch';

gulp.task('compile-css', function(){
    return gulp.src('./source/css/main.styl')
        .pipe(stylus({compress: true}))
        .pipe(gulp.dest('./public/css/'));
});

gulp.task('serve', function() {
    watch('./source/css/**/*.styl', function(){
        gulp.start('compile-css');
    });
});