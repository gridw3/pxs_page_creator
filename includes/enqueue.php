<?php

function pxs_enqueue() {
    wp_register_style('pxs_bootstrap_css', plugins_url('/public/css/bootstrap.min.css', __FILE__));
    wp_enqueue_style('pxs_bootstrap_css');

    wp_register_style('pxs_main_css', plugins_url('/public/css/main.css', __FILE__));
    wp_enqueue_style('pxs_main_css');
}